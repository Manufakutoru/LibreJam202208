# Push to Kill

You are stranded on a Planet and aliens start to chase you. As it seems the energy of your weapons projectiles makes the aliens replicate instead of killing them. But the impact still pushes them back a little.  
Try to push the aliens into the traps and suvive as long as possible.

### Controls

W,A,S,D - move around  
Left Mouse Button - shoot at aliens

## Build

To build the game install Godot3.5, open the project and either run in directly or export it for your system.

## License

The code is under AGPL3 and the Assets are CC-BY-SA 4.0.

## External Stuff

used shader from McSpider: https://gist.github.com/McSpider/c8c693f1065fdb5ec44c64cb9fdac340
