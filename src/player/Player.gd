extends Area2D

signal player_is_dead

var state
var mid_point = Vector2(0, 2000)

export var speed = 100
enum {INIT, WALKING, DEAD}

func _ready():
	change_state(INIT)

func _process(delta):
	match state:
		WALKING:
			var velocity = Vector2.ZERO
			if Input.is_action_pressed("move_right"):
				velocity.x += 1
			if Input.is_action_pressed("move_left"):
				velocity.x -= 1
			if Input.is_action_pressed("move_down"):
				velocity.y += 1
			if Input.is_action_pressed("move_up"):
				velocity.y -= 1

			if velocity != Vector2.ZERO:
				velocity = velocity.normalized() * speed
				position += velocity * delta
				$AnimatedSprite.set_animation("walk")
			else:
				$AnimatedSprite.set_animation("stand")
				
			$Weapon.look_at(get_global_mouse_position())
			var rot = fmod($Weapon.rotation-PI/2, 2*PI)
			$AnimatedSprite.set_flip_h(rot<0 and rot>-PI or rot>PI and rot<2*PI)
			if $AnimatedSprite.flip_h:
				$Weapon.position.x = 2.71
			else:
				$Weapon.position.x = -2.71
			z_index = global_position.y
		DEAD:
			z_index = mid_point.y + 1000
	
	if (position - mid_point).length() > 1000:
		die()

func change_state(new_state):
	state = new_state
	match new_state:
		INIT:
			$CollisionShape2D.set_deferred("disabled", true)
			$Weapon.visible = false
			$AnimatedSprite.set_animation("stand")
			$AnimatedSprite.set_playing(true)
		WALKING:
			$CollisionShape2D.set_deferred("disabled", false)
			$Weapon.visible = true
			$AnimatedSprite.set_animation("walk")
			$AnimatedSprite.set_playing(true)
		DEAD:
			$CollisionShape2D.set_deferred("disabled", true)
			$Weapon.visible = false
			$AnimatedSprite.set_flip_h(false)
			$AnimatedSprite.set_animation("die")
			$AnimatedSprite.set_playing(true)

func _on_Player_body_entered(body):
	if body.is_in_group("enemies") and state==WALKING:
		die()

func die():
	emit_signal("player_is_dead")
	change_state(DEAD)
