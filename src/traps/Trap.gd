extends Area2D

func _on_Trap_body_entered(body):
	if body.is_in_group("enemies"):
		body.die()

func _on_Trap_area_entered(area):
	if area.is_in_group("player"):
		area.die()
