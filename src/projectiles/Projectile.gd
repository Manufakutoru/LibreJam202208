extends Area2D

var speed = 400
var push_strength = 400
var mid_point = Vector2(0, 2000)

func start(pos, rot):
	$AnimatedSprite.play("flying")
	position = pos
	rotation = rot

func _process(delta):
	position += Vector2.RIGHT.rotated(rotation) * speed * delta
	if (position - mid_point).length() > 1000:
		print_debug("delete projectile")
		queue_free()

func _on_Projectile_body_entered(body):
	if body.is_in_group("enemies"):
		body.get_shot(push_strength * Vector2.RIGHT.rotated(rotation))
		queue_free()
		
