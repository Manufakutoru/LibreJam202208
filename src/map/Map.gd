extends Node2D

export (PackedScene) var EnemyScene
export (PackedScene) var ProjectileScene

var weapon_cd_time = 0.2
var enemy_spawn_time = 0.75
var enemy_number = 0
var highscore = 0
var score = 0.0
var mid_point = Vector2(0, 2000)

func _ready():
	$Player/Scores/HighScore.text = "HIGHSCORE: " + str(highscore)
	$Player/Scores/Score.text = "SCORE: " + str(int(score))
	$Player.connect("player_is_dead", self, "_on_player_is_dead")
	start_game()

func start_game():
	for enemy in $enemies.get_children():
		enemy.queue_free()
	$Player.position = mid_point + Vector2.LEFT * 500
	$Player.change_state($Player.INIT)
	yield(get_tree().create_timer(1), "timeout")
	score = 0
	$Player.change_state($Player.WALKING)
	$SpawnTimer.start(enemy_spawn_time)

func spawn_enemy(spawn_pos, velocity=Vector2.ZERO, hue_shift=randf(), is_h_flipped=randi()%2):
	enemy_number += 1
	var enemy = EnemyScene.instance()
	$enemies.add_child(enemy)
	enemy.connect("divide", self, "spawn_enemy")
	enemy.start($Player, spawn_pos, velocity, hue_shift, is_h_flipped)

func _on_SpawnTimer_timeout():
	var spawn_pos = $Player.position + Vector2.UP.rotated(randf()*2*PI) * 600
	while (mid_point - spawn_pos).length() > 900:
		spawn_pos = $Player.position + Vector2.UP.rotated(randf()*2*PI) * 600
	spawn_enemy(spawn_pos)

func _process(delta):
	if $Player.state == $Player.WALKING:
		score += delta
		$Player/Scores/Score.text = "SCORE: " + str(int(score))
		if Input.is_action_pressed("shoot"):
			if $WeaponTimer.is_stopped():
				shoot()
				$WeaponTimer.start(weapon_cd_time)

func shoot():
	var projectile = ProjectileScene.instance()
	$projectiles.add_child(projectile)
	projectile.start($Player/Weapon/Spawn.global_position, $Player/Weapon/Spawn.global_rotation)	

func _on_player_is_dead():
	$SpawnTimer.stop()
	if score > highscore:
		highscore = int(score)
	$Player/Scores/HighScore.text = "HIGHSCORE: " + str(highscore)
	yield(get_tree().create_timer(5), "timeout")
	start_game()

func _draw():
	draw_circle(mid_point, 2000, Color.black)
	draw_circle(mid_point, 1000, Color.white)
	draw_circle(mid_point,  990, Color.black)
