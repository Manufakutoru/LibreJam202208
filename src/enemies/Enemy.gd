extends RigidBody2D

signal divide

var state = null
var target
var hue_shift: float
var energy = 0
var energy_limit = 1
var flip_h: bool setget set_flip_h
var mid_point = Vector2(0, 2000)

export (float) var speed = 150

enum {INIT, MOVING, DEAD, DIVISION}

func _ready():
	#set_friction(0)
	var material = PhysicsMaterial.new()
	material.friction = 0.0
	material.bounce = 0.2
	set_physics_material_override(material)
	$AnimatedSprite.set_material($AnimatedSprite.get_material().duplicate(true))
	#
	change_state(INIT)

func start(_target, pos, vel, _hue_shift, is_h_flipped):
	hue_shift = _hue_shift
	set_flip_h(is_h_flipped)
	#
	set_position(pos)
	$AnimatedSprite.material.set_shader_param("Shift_Hue", hue_shift)
	#
	target = _target
	#
	yield(get_tree().create_timer(0.5+0.5*randf()), "timeout")
	change_state(MOVING)
	set_linear_velocity(vel)

func set_flip_h(_flip_h):
	flip_h = _flip_h
	$AnimatedSprite.set_flip_h(flip_h)
	if flip_h:
		$AnimatedSprite.position.x = 3
	else:
		$AnimatedSprite.position.x = -3

func _physics_process(delta):
	match state:
		MOVING:
			var target_dir = (target.position - position).normalized()
			apply_central_impulse(target_dir * speed * delta)
			#
			set_flip_h(target_dir.x > 0)
	
func _process(delta):
	if (position - mid_point).length() > 1000:
		die()
	z_index = global_position.y

func change_state(new_state):
	state = new_state
	match new_state:
		INIT:
			$AnimatedSprite.set_animation("move")
			$AnimatedSprite.set_playing(false)
		MOVING:
			$AnimatedSprite.set_animation("move")
			$AnimatedSprite.set_playing(true)
		DEAD:
			set_mode(RigidBody2D.MODE_STATIC)
			set_linear_velocity(Vector2.ZERO)
			$CollisionShape2D.set_deferred("disabled", true)
			$AnimatedSprite.set_animation("die")
			$AnimatedSprite.set_playing(true)
		DIVISION:
			$AnimatedSprite.set_animation("divide")
			$AnimatedSprite.set_playing(true)


func die():
	change_state(DEAD)
	yield($AnimatedSprite, "animation_finished")
	call_deferred("queue_free")

func get_shot(impulse_vector):
	if state != DEAD:
		if $AnimatedSprite.animation == "move":
			apply_central_impulse(impulse_vector)
		energy += 1
		if state == MOVING:
			$AnimatedSprite.material.set_shader_param("Shift_Hue", 0)
			$AnimatedSprite.set_animation("hit")
			$AnimatedSprite.set_playing(true)
			yield($AnimatedSprite, "animation_finished")
			$AnimatedSprite.material.set_shader_param("Shift_Hue", hue_shift)	
			change_state(MOVING)
			if energy >= energy_limit:
				divide()
				energy = 0

func divide():
	change_state(DIVISION)
	yield($AnimatedSprite, "animation_finished")
	var spawn_pos = $Spawn.position
	if flip_h:
		spawn_pos.x *= -1
	spawn_pos += position
	emit_signal("divide", spawn_pos, linear_velocity, hue_shift * (1+(randf()-0.5)*0.1), flip_h)
	if state != DEAD:
		change_state(INIT)
	yield(get_tree().create_timer(0.5+0.5*randf()), "timeout")
	if state != DEAD:
		change_state(MOVING)
